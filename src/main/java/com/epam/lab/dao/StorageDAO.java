package com.epam.lab.dao;

import com.epam.lab.model.FileEnt;
import com.epam.lab.persist.DirData;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

public class StorageDAO {
  private DirData storage;

  StorageDAO() {
    storage = DirData.getInstance();
  }

  public List<Path> showAllFiles() throws IOException {
    return storage
        .prepareDirTree()
        .get()
        .collect(Collectors.toList());
  }

  public boolean delete(Path path) throws IOException {
    return Files.deleteIfExists(storage.getStorePaths().resolve(path));
  }

  public boolean create(byte[] fileContent, Path path) throws IOException{
    ByteBuffer buff = ByteBuffer.wrap(fileContent);
    buff.rewind();
    try(SeekableByteChannel target = Files.newByteChannel(storage.getStorePaths().resolve(path),
                                                          StandardOpenOption.WRITE,
                                                          StandardOpenOption.CREATE_NEW)) {
      target.write(buff);
    }
    return true;
  }

  public FileEnt getFile(Path fPath) throws IOException {
    Path filePath = storage.getStorePaths().resolve(fPath);
    ByteBuffer output = ByteBuffer.allocate((int) Files.size(filePath));
    try(SeekableByteChannel target = Files.newByteChannel(filePath,
                                                          StandardOpenOption.READ)) {
      target.read(output);
    }
    output.rewind();
    return new FileEnt(filePath.getFileName().toString(), output.array());
  }
}
