package com.epam.lab.web.soap;

import com.epam.lab.bus.Storable;
import com.epam.lab.bus.StorageOps;
import com.epam.lab.model.FileEnt;
import com.epam.lab.web.exception.FileExists;
import com.epam.lab.web.exception.NoSuchFile;
import java.util.List;
import javax.jws.WebService;

@WebService(endpointInterface = "com.epam.lab.web.soap.FileStorage")
public class FileStorageImpl implements FileStorage{

  private Storable storage = new StorageOps();

  @Override
  public boolean removeFile(String fName) throws NoSuchFile{
    return storage.removeFile(fName);
  }

  @Override
  public FileEnt download(String fName) throws NoSuchFile {
    return storage.getFile(fName);
  }

  @Override
  public List<String> getFileList() {
    return storage.getDirList();
  }

  @Override
  public boolean uploadFile(byte[] fBuff, String fName) throws FileExists {
    return storage.addFile(fBuff, fName);
  }
}
