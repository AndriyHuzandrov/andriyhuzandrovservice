package com.epam.lab.web.soap;

import com.epam.lab.model.FileEnt;
import com.epam.lab.web.exception.FileExists;
import com.epam.lab.web.exception.NoSuchFile;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface FileStorage {

  @WebMethod
  boolean removeFile(String fName) throws NoSuchFile;

  @WebMethod
  FileEnt download(String fName) throws NoSuchFile;

  @WebMethod
  List<String> getFileList();

  @WebMethod
  boolean uploadFile(byte[] fBuff, String fName) throws FileExists;


}
