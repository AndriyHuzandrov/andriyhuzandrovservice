package com.epam.lab.web.exception;

import javax.xml.ws.WebFault;

@WebFault
public class FileExists extends Exception {

  public FileExists() {
    super("File already exists in the storage, or its size unacceptable");
  }

  public FileExists(String message) {
    super(message);
  }
}
