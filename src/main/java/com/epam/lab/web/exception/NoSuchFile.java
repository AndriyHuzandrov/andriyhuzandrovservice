package com.epam.lab.web.exception;

import javax.xml.ws.WebFault;

@WebFault
public class NoSuchFile extends Exception {

  public NoSuchFile() {
    super("File does not exist");
  }

  public NoSuchFile(String message) {
    super(message);
  }
}
