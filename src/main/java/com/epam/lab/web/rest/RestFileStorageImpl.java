package com.epam.lab.web.rest;

import com.epam.lab.bus.Storable;
import com.epam.lab.bus.StorageOps;
import com.epam.lab.model.FileEnt;
import com.epam.lab.web.exception.FileExists;
import com.epam.lab.web.exception.NoSuchFile;
import java.util.Objects;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class RestFileStorageImpl implements  RestFileStorage {

  private Storable storage = new StorageOps();

  @Override
  public Response getFileList() {
    return Response.ok().entity(storage.getDirList()).build();
  }

  @Override
  public Response getFile(String name) {
    FileEnt fileToGet;
    try {
      fileToGet = storage.getFile(name);
    } catch (NoSuchFile e) {
      return Response.status(Response.Status.NOT_FOUND).entity(e.getMessage()).build();
    }
    return Response.ok().entity(fileToGet).build();
  }

  @Override
  public Response uploadFile(FileEnt upFile) {
    try {
      if(Objects.isNull(upFile)) {
        throw new IllegalArgumentException("Can not read from nothing");
      }
      storage.addFile(upFile.getFileContent(), upFile.getFileName());
    } catch (FileExists  | IllegalArgumentException e) {
      return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
    return Response.ok().build();
  }

  @Override
  public Response deleteFile(String name) {
    try {
      storage.removeFile(name);
    } catch (NoSuchFile e) {
      return Response.status(Status.NOT_FOUND).entity(e.getMessage()).build();
    }
    return Response.ok().build();
  }
}
