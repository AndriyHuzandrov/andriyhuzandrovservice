package com.epam.lab.web.rest;

import com.epam.lab.model.FileEnt;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/engineREST")
public interface RestFileStorage {

  @GET
  @Path("/files")
  @Produces("application/json; charset=UTF-8")
  Response getFileList();

  @GET
  @Path("/files/{fileName}")
  @Consumes("text/plain; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response getFile(@PathParam("fileName") String name);

  @POST
  @Path("/files")
  @Consumes("application/json; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response uploadFile(FileEnt upFile);

  @DELETE
  @Path("/files/{fileName}")
  @Consumes("text/plain; charset=UTF-8")
  @Produces("application/json; charset=UTF-8")
  Response deleteFile(@PathParam("fileName") String name);

}
