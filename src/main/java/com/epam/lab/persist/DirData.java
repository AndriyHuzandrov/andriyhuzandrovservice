package com.epam.lab.persist;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirData {
  private static Path storePaths;
  private static List<Path>  dirList;
  private static volatile DirData instance;

  private DirData() {
    dirList = new CopyOnWriteArrayList<>();
    storePaths = Paths.get("ServerStorage");
  }

  public static DirData getInstance() {
    if(instance == null) {
      synchronized (DirData.class) {
        if(instance == null) {
          instance = new DirData();
        }
      }
    }
    return instance;
  }

  public Supplier<Stream<Path>> prepareDirTree() throws IOException {
    dirList = Files
        .walk(storePaths.resolve("./"))
        .collect(Collectors.toList());
    return () -> dirList.stream();
  }

  public Path getStorePaths() {
    return storePaths;
  }

}
