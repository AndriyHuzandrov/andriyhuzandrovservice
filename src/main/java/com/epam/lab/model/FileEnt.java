package com.epam.lab.model;

import javax.xml.bind.DatatypeConverter;

public class FileEnt {
  private String fileName;
  private String fileContent;

  public FileEnt() {
  }

  public FileEnt(String fileName, byte[] fContent) {
    this.fileName = fileName;
    this.fileContent = DatatypeConverter.printBase64Binary(fContent);
  }

  public String getFileName() {
    return fileName;
  }

  public byte[] getFileContent() {
    return DatatypeConverter.parseBase64Binary(fileContent);
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setFileContent(byte[] fContent) {
    this.fileContent = DatatypeConverter.printBase64Binary(fContent);
  }
}

