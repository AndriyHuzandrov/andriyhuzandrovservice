package com.epam.lab.bus;

import com.epam.lab.dao.DAOs;
import com.epam.lab.dao.StorageDAO;
import com.epam.lab.model.FileEnt;
import com.epam.lab.web.exception.FileExists;
import com.epam.lab.web.exception.NoSuchFile;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class StorageOps implements Storable {
  private static final Logger serverLog = LogManager.getLogger(StorageOps.class);
  private double maxFileSize;
  private StorageDAO storage;

  public StorageOps() {
    maxFileSize = 1E+6;
    storage = DAOs.createStorageDAO();
  }

  @Override
  public List<String> getDirList() {
    List<String> dirList = new ArrayList<>();
    try {
      dirList = storage
          .showAllFiles()
          .stream()
          .skip(1)
          .map(Path::getFileName)
          .map(Path::toString)
          .collect(Collectors.toList());
    } catch (IOException e) {
      serverLog.error("Error while reading directory");
    }
    return dirList;
  }

  @Override
  public FileEnt getFile(String fName) throws NoSuchFile{
    if(isPresent(fName)) {
      serverLog.trace(fName + " found, prepare to download\n");
      StorageDAO storage = DAOs.createStorageDAO();
      Path fPath = correctFilePath(fName);
      try {
        return new FileEnt(fPath.getFileName().toString(),
            storage.getFile(fPath).getFileContent());

      } catch (IOException e) {
        serverLog.error("Error while reading file" + fName);
      }
    }
    throw new NoSuchFile();
  }

  @Override
  public boolean removeFile(String fName) throws NoSuchFile {
    if(isPresent(fName)) {
      serverLog.trace(fName + " found and will be deleted\n");
      StorageDAO storage = DAOs.createStorageDAO();
      Path targetPath = correctFilePath(fName);
      try {
        return storage.delete(targetPath);
      } catch (IOException e) {
        serverLog.error(fName + " file deleting failed");
        return false;
      }

    }
    throw new NoSuchFile();
  }

  @Override
  public boolean addFile(byte[] fContent, String fName) throws FileExists {
    if(!isPresent(fName) && isGoodSize(fContent)) {
      Path targetPath = correctFilePath(fName);
      serverLog.trace(fName + " name and size is OK.\n Prepare to add the file to storage");
      StorageDAO storage = DAOs.createStorageDAO();
      try {
        return storage.create(fContent, targetPath);
      } catch (IOException e) {
        serverLog.error(fName + " file adding failed");
        return false;
      }

    }
    throw new FileExists();
  }

  private boolean isGoodSize(byte[] fBuff) {
    serverLog.trace("Checking if uploaded file has appropriate size");
    return fBuff.length <= maxFileSize & fBuff.length > 0;
  }

  private Path correctFilePath(String fName) {
    serverLog.trace(fName + " is transformed to a valid path");
    Path filePath;
    if(fName.matches("^(\\.\\./)+.+\\..*")) {
      fName = fName.substring(fName.lastIndexOf("/")+1);
    }
    filePath = Paths.get(fName);
    return filePath;
  }

  private boolean isPresent(String fName) {
    double fileCount = getDirList()
        .stream()
        .filter(f -> f.contains(fName))
        .count();
    return fileCount > 0;
  }

}
