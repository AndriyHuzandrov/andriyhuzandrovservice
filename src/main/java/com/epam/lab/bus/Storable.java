package com.epam.lab.bus;

import com.epam.lab.model.FileEnt;
import com.epam.lab.web.exception.FileExists;
import com.epam.lab.web.exception.NoSuchFile;
import java.util.List;

public interface Storable {

  List<String> getDirList();
  FileEnt getFile(String f) throws NoSuchFile;
  boolean removeFile(String f) throws NoSuchFile;
  boolean addFile(byte[] fBuff, String fName) throws FileExists;

}
